<?php

use Illuminate\Database\Migrations\Migration;
use Jenssegers\Mongodb\Schema\Blueprint;

class CreateItemsIndexes extends Migration
{
    protected $connection = 'mongodb';

    public function up()
    {
        Schema::connection($this->connection)->create('items', function (Blueprint $collection) {
            $collection->unique('artno');
            $collection->index('price');
            $collection->index('type');
        });

        Schema::connection($this->connection)->create('import_results', function (Blueprint $collection) {
            $collection->index('import_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('items');
    }
}
