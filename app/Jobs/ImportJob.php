<?php

namespace App\Jobs;

use App\Services\ImportHandlerService;
use App\Services\ImportService;
use App\Services\ItemService;
use SplFileObject;

class ImportJob extends Job
{
    private int $from;
    private int $to;
    private string $filePath;
    private string $importId;

    public function __construct(string $importId, string $filePath, int $from, int $to)
    {
        $this->importId = $importId;
        $this->filePath = '/var/www/public/' . $filePath;
        $this->from = $from - 1;
        $this->to = $to;
    }

    public function handle(ImportHandlerService $importHandlerService, ItemService $itemService, ImportService $importService)
    {
        $file = new SplFileObject($this->filePath, "r");
        $validatedLinesBatch = [];

        $idx = $this->from;
        $file->seek($idx);
        do {
            $line = $file->current();

            $handledLine = $importHandlerService->handleLine($line);
            if ($handledLine) {
                $validatedLinesBatch[] = $handledLine;
            } else {
                $importService->addInvalidLine($this->importId, $idx);
            }

            $idx++;
            $file->next();
        } while ($idx < $this->to);

        $itemService->create($validatedLinesBatch, $this->importId);
    }
}
