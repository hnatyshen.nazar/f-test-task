<?php

namespace App\Listeners;

use App\Events\ImportFinishedEvent;
use App\Services\ImportService;

class ImportFinishedListener
{
    /**
     * @var ImportService
     */
    private ImportService $importService;

    public function __construct(ImportService $importService)
    {
        $this->importService = $importService;
    }

    public function handle(ImportFinishedEvent $event)
    {
        $this->importService->finishImport($event->getImportId());
    }
}
