<?php

namespace App\Repositories;

use App\Models\Import;
use App\Models\ImportResults;

class ImportRepository extends BaseMongoRepository
{
    protected function getModelClass(): string
    {
        return Import::class;
    }

    public function increaseImportedItems(string $importId, int $count): ?ImportResults
    {
        $importResults = $this->getImportResults($importId);

        $importResults->query()->where('import_id', $importId)->update([
            '$inc' => [
                'imported_items_count' => $count,
            ]
        ]);

        return $importResults;
    }

    public function addInvalidLine(string $importId, int $lineIdx): void
    {
        $importResults = $this->getImportResults($importId);
        $importResults->push('invalid_items_lines', $lineIdx, true);

        $importResults->query()->where('import_id', $importId)->update([
            '$inc' => [
                'invalid_items_count' => 1,
            ],
        ]);
    }

    private function getImportResults(string $importId): ?ImportResults
    {
        $import = $this->getOne($importId);

        return $import->importResults;
    }

    public function getAll()
    {
        $model = $this->getModelInstance();

        return $model->all();
    }
}
