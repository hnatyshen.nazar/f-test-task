<?php

namespace App\Repositories;

use App\Models\Item;
use Illuminate\Support\Facades\DB;

class ItemRepository extends BaseMongoRepository
{

    protected function getModelClass(): string
    {
        return Item::class;
    }

    public function insertBatch(array $data): void
    {
        DB::connection('mongodb')->table('items')->insert($data);
    }

    public function getByMinPriceAndType(?int $price, ?int $type, int $limit)
    {
        $model = $this->getModelInstance();
        if (!empty($price)) {
            $model = $model->where('price', '>', $price);
        }
        if (!empty($type)) {
            $model = $model->where('type', $type);
        }

        return $model->limit($limit)->get();
    }
}
