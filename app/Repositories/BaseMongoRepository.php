<?php

namespace App\Repositories;

use Jenssegers\Mongodb\Eloquent\Model;
use RuntimeException;

abstract class BaseMongoRepository
{
    protected function getModelInstance(): Model
    {
        $class = $this->getModelClass();

        return (new $class());
    }

    abstract protected function getModelClass(): string;

    /**
     * @param array $data
     * @return Model|null
     */
    public function create(array $data): ?Model
    {
        $model = $this->getModelInstance();
        $model->fill($data);

        if ($model->save()) {
            return $model;
        }

        throw new RuntimeException('Cannot create model ' . $this->getModelClass());
    }

    public function update(Model $model, array $data): ?Model
    {
        $model->fill($data);

        if ($model->save()) {
            return $model;
        }

        throw new RuntimeException('Cannot update model ' . $this->getModelClass());
    }

    public function getOne($value, $attribute = '_id'): ?Model
    {
        $model = $this->getModelInstance();

        return $model->where($attribute, '=', $value)->first();
    }
}
