<?php

namespace App\Services;

use App\Events\ImportFinishedEvent;
use App\Repositories\ImportRepository;
use App\Repositories\ItemRepository;

class ItemService
{
    /**
     * @var ItemRepository
     */
    private ItemRepository $itemRepository;
    /**
     * @var ImportRepository
     */
    private ImportRepository $importRepository;

    public function __construct(ItemRepository $itemRepository, ImportRepository $importRepository)
    {
        $this->itemRepository = $itemRepository;
        $this->importRepository = $importRepository;
    }

    public function create(array $data, string $importId)
    {
        $this->itemRepository->insertBatch($data);
        $this->importRepository->increaseImportedItems($importId, count($data));

        $freshImportResults = $this->importRepository->getOne($importId)->importResults;

        if ($freshImportResults->imported_items_count + $freshImportResults->invalid_items_count >= $freshImportResults->total_items_count) {
            event(new ImportFinishedEvent($importId));
        }
    }

    public function getItemsByMinPriceAndType(?int $price, ?int $type, int $limit)
    {
        return $this->itemRepository->getByMinPriceAndType($price, $type, $limit);
    }
}
