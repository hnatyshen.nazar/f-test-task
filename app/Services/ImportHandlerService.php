<?php

namespace App\Services;

use App\Models\ItemTypes;
use Illuminate\Validation\Rule;

class ImportHandlerService
{
    public function handleLine(string $line): ?array
    {
        $data = json_decode($line, true);
        $data = array_change_key_case($data, CASE_LOWER);

        if (!$this->validate($data)) {
            return null;
        }

        $data['title'] = mb_strtolower($data['title']);

        return $data;
    }

    private function validate(array $data): bool
    {
        $validator = \Validator::make($data, [
            'artno' => 'required|integer',
            'title' => 'required|string',
            'price' => 'required|integer',
            'type' => ['required', 'integer', Rule::in(ItemTypes::getTypes())],
        ]);

        return !$validator->fails();
    }
}
