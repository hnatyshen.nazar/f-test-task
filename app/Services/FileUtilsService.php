<?php

namespace App\Services;

use SplFileObject;

class FileUtilsService
{
    public function getLinesCount(string $filePath): int
    {
        $file = new SplFileObject($filePath, 'r');

        $file->seek(PHP_INT_MAX);

        return $file->key() + 1;
    }

    public function getSpecifiedLine(SplFileObject $file, int $lineIdx): ?string
    {
        $file->seek($lineIdx);

        return $file->current() ?? null;
    }
}
