<?php

namespace App\Services;

use App\Models\Import;
use App\Repositories\ImportRepository;
use Jenssegers\Mongodb\Eloquent\Model;

class ImportService
{
    /**
     * @var ImportRepository
     */
    private ImportRepository $importRepository;

    public function __construct(ImportRepository $importRepository)
    {
        $this->importRepository = $importRepository;
    }

    public function initImport(int $lines): ?Model
    {
        $import = $this->importRepository->create([
            'status' => Import::STATUS_STARTED,
        ]);

        $import->importResults->total_items_count = $lines;
        $import->importResults->save();

        return $import;
    }

    public function finishImport(string $importId): void
    {
        $import = $this->importRepository->getOne($importId);

        $this->importRepository->update($import, [
            'status' => Import::STATUS_FINISHED,
        ]);
    }

    public function addInvalidLine(string $importId, int $lineIdx): void
    {
        $this->importRepository->addInvalidLine($importId, $lineIdx);
    }

    public function get()
    {
        return $this->importRepository->getAll();
    }
}
