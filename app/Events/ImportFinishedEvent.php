<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class ImportFinishedEvent extends Event
{
    use SerializesModels;

    private string $importId;

    public function __construct(string $importId)
    {
        $this->importId = $importId;
    }

    /**
     * @return string
     */
    public function getImportId(): string
    {
        return $this->importId;
    }
}
