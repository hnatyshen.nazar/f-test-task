<?php

namespace App\Http\Controllers;

use App\Jobs\ImportJob;
use App\Models\Import;
use App\Models\Item;
use App\Services\FileUtilsService;
use App\Services\ImportService;
use App\Services\ItemService;
use Illuminate\Http\Request;
use SplFileObject;

class ExampleController extends Controller
{
    /**
     * @var FileUtilsService
     */
    private $fileUtilsService;
    /**
     * @var ImportService
     */
    private ImportService $importService;
    /**
     * @var ItemService
     */
    private ItemService $itemService;

    public function __construct(FileUtilsService $fileUtilsService, ImportService $importService, ItemService $itemService)
    {
        $this->fileUtilsService = $fileUtilsService;
        $this->importService = $importService;
        $this->itemService = $itemService;
    }

    public function home()
    {
        $imports = $this->importService->get();

        foreach ($imports as $import) {
            $results = $import->importResults;
            printf('id: %s, total items: %d, imported items: %d, invalid items: %d<br>', $import->_id, $results->total_items_count, $results->imported_items_count, $results->invalid_items_count);
        }
    }

    public function parse()
    {
        $filePath = 'test_exercise.jsonl';
        $lines = $this->fileUtilsService->getLinesCount($filePath);
        $import = $this->importService->initImport($lines);
        $chunk = 1000;
        for ($i = 1; $i <= $lines; $i += $chunk+1) {
            $last = $i + $chunk > $lines ? $lines : $i + $chunk;
            dispatch(new ImportJob($import->_id, $filePath, $i, $last));
        }
    }

    public function show(Request $request)
    {
        dd($this->itemService->getItemsByMinPriceAndType($request->get('price') ?? null, $request->get('type') ?? null, $request->get('limit') ?? 10));
    }
}
