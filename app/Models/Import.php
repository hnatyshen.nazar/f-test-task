<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Import extends Model
{
    public const STATUS_STARTED = 1;
    public const STATUS_FINISHED = 2;
    public $timestamps = false;
    protected $connection = 'mongodb';
    protected $collection = 'imports';
    protected $table = 'imports';
    protected $fillable = ['status', 'type'];

    public function importResults()
    {
        return $this->hasOne(ImportResults::class, 'import_id')->withDefault([
            'import_id' => $this->_id,
            'imported_items_count' => 0,
            'total_items_count' => 0,
            'invalid_items_count' => 0,
            'invalid_items_lines' => [],
        ]);
    }
}
