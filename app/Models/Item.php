<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Item extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'items';
    protected $table = 'items';
    protected $fillable = [
        'artno',
        'title',
        'price',
        'type',
    ];
    public $timestamps = false;
}
