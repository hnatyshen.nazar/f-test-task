<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class ImportResults extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'import_results';
    protected $table = 'import_results';
    protected $fillable = [
        'import_id',
        'imported_items_count',
        'total_items_count',
        'invalid_items_count',
        'invalid_items_lines',
    ];
    public $timestamps = false;

    public function import()
    {
        return $this->belongsTo(Import::class, 'import_id', '_id');
    }
}
