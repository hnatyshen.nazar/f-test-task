<?php

namespace App\Models;

class ItemTypes
{
    public const TYPE_1 = 1;
    public const TYPE_2 = 2;
    public const TYPE_3 = 3;
    public const TYPE_4 = 4;

    public static function getTypes(): array
    {
        return [
            self::TYPE_1,
            self::TYPE_2,
            self::TYPE_3,
            self::TYPE_4,
        ];
    }
}
