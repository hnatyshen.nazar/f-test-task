# Task
There is jsonl file with 100k items (1 item = 1 line).
Preconditions: MongoDB, Laravel/Lumen, async queues (amount of workers > 5)

1. Parse the file (create import in the database) and run async jobs to handle items from the file
2. Add statuses for import: started / finished, set 'finished' status when all items are handled
3. Run jobs async (store items, transform title to lowercase, validate artno: don't store the items without artno)
4. Store import stats: items amount, invalid items amount, id's for invalid items. Relate stats to import_id.
5. Add methods to get the amount of the items
5.1 item.price > price
5.2 item.type == type

Item
{"artno": 1, "TITLE": "VC1EZDXLZJY5BSS7", "price": 100, "type": 1}
artno: int
title: string
price: int
type: int. Available types: 1,2,3,4

# Short explanation of how I resolved it
This solution can handle/transform the one JSON file with 100K lines in 8-9 seconds and save the RAM.

Tools: Lumen + MySQL (for laravel migrations) + Redis (queue driver) + supervisor (15 queue workers, configured in numprocs). To read files quicker - SplFileObject, to write in MongoDB faster - $inc for items_amount and batch insert to write chunks of items.
File for parsing stored in /public path.
jenssegers/mongodb using as MongoDB adapter to save time on development

How does it work?
1. Count the lines in the file, divide them into chunks, and put them into jobs.
2. In jobs we validate and transform line to the needed format of the array, if validation passed - write a new item in mongo + increment items_count, if not - increment failed_lines + write the line idx.
3. When imported+failed>=total lines then trigger `ImportFinishedEvent` 

p.s. it can be faster without "moloquent", but as is

# Installation
1. cp `.env.example .env`
1. `make docker_build`
2. `make composer_install`
3. generate artisan key. In lumen we don't have such a command, so do it by yourself
4. `make db_migrate`
5. `make docker_up`

# Test routes
/show (optional get-param `type`, `price` and `limit`=10 by default)

/parse run parsing and create jobs

/ (homepage) - here you can see the import stats
