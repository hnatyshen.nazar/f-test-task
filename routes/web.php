<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//$router->get('/', function () use ($router) {
//    app('redis')->set('test', 1488);
//    dispatch(new \App\Jobs\ExampleJob());
//    return $router->app->version();
//});

$router->get('/', 'ExampleController@home');
$router->get('/parse', 'ExampleController@parse');
$router->get('/show', 'ExampleController@show');
